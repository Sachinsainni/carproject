module.exports = function(inventory)
{
    if(!inventory){
        return[];
    }
    let BMWandAudi = [];
    for (let i = 0; i < inventory.length; i++)
    {
        if ((inventory[i].car_make === 'BMW') || (inventory[i].car_make === 'Audi'))
        {
            BMWandAudi.push(inventory[i]);
        }
    }
    return BMWandAudi;
}