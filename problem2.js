module.exports = function(inventory)
{
    if(!inventory){
        return[];
    }
    let carindex = inventory.length - 1;
    let car = inventory[carindex];
    let carDetails = 'Last car is a ' + car.car_make + ' ' + car.car_model;
    return carDetails;
}