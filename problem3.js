module.exports = function(inventory)
{
    if(!inventory){
        return[];
    }
    let sortInventory = inventory.sort((a, b) => a.car_model > b.car_model ? 1 : -1);
    return sortInventory;
}