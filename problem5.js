module.exports = function(inventory)
{
    if(!inventory){
        return[];
    }
    let Before2000 = [];
    for (let i = 0; i < inventory.length; i++)
    {
        if (inventory[i] < 2000)
        {
            Before2000.push(inventory[i]);
        }
    }
    return Before2000;
}